# This file contains Julia ports of some Matlab functions

function ndgrid(x::AbstractVector{T}, y::AbstractVector{T}) where T <: Number
    X = [i for i in x, j in 1:length(y)]
    Y = [j for i in 1:length(x), j in y]
    return X, Y
end

function meshgrid(x::AbstractVector{T}, y::AbstractVector{T}) where T <: Number
    X = [i for j in 1:length(y), i in x]
    Y = [j for j in y, i in 1:length(x)]
    return X, Y
end