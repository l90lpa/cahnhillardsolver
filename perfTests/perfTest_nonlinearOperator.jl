using BenchmarkTools
using Profile, PProf

include("../ch_nonlinearOperator.jl")

FFTW.set_num_threads(1);

dealias=true;
n=64;
a = Vector{ComplexF64}(1:n^2);
a = reshape(a,(n,n));

N=CH_NLOperator(n,dealias);
NCached=CH_NLOperatorCached(n,dealias);

function notCached(A)   
    A=map(N,A);
end

function cached(A)
    A=map(NCached,A);    
end

io = IOContext(stdout)

notCached(a)
b2 = @benchmark notCached(A) setup=(A=a); show(io, MIME("text/plain"), b2); print("\n\n")

cached(a)
b1 = @benchmark cached(A) setup=(A=a); show(io, MIME("text/plain"), b1);