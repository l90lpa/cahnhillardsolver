using Random
using BenchmarkTools
using Profile, PProf

include("../ch_linearOperator.jl")

function step(L,A,B,C)   
    dt=1e-5;
    dt2 = dt/2;
    return ((dt2 * L .+ 1) .* C + dt2 * (3 * A - B)) ./ (-dt2 * L .+ 1);
end

function step_fullBroadcast(L,A,B,C)   
    dt=1e-5;
    dt2 = dt/2;
    return ((((L .* dt2) .+ 1) .* C) .+ ((A .* 3) .- B) .* dt2) ./ ((L .* (-dt2)) .+ 1);
end

dealias=true;
n=64;
l = constructLaplacianOperator(n)
rng = MersenneTwister(257);
a = rand(rng, n, n);
b = rand(rng, n, n);
c = rand(rng, n, n);

T1 = step(l,a,b,c);
T2 = step_fullBroadcast(l,a,b,c);
print(T1==T2)
print("\n\n")

io = IOContext(stdout)

step(l,a,b,c)
b1 = @benchmark step(L,A,B,C) setup=(L=l;A=a;B=b;C=c); show(io, MIME("text/plain"), b1); print("\n\n")

step_fullBroadcast(l,a,b,c)
b2 = @benchmark step_fullBroadcast(L,A,B,C) setup=(L=l;A=a;B=b;C=c); show(io, MIME("text/plain"), b2)

