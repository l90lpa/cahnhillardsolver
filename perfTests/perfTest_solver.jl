using Random
using JLD2
using FileIO
using Profile, PProf
using BenchmarkTools

include("../deprecated/ch.jl")
include("../ch_problem.jl")

function initialCondition(X::Matrix{T}, Y::Matrix{T}) where T <: Number
    seed = 257;
    rng = MersenneTwister(257);
    return 0.01*(2*rand(rng, size(X,1),size(X,2)).-1);
end

nu=0.01;
dt=1e-5;
N=64;
tMax=0.251;
dealias=true;
dataCollectionInterval=0.25;

cahnHillardGalerkin(nu,dt,N,4*dt,initialCondition,dealias,dataCollectionInterval)
@time cahnHillardGalerkin(nu,dt,N,tMax,initialCondition,dealias,dataCollectionInterval)

ch_problem(nu,dt,N,4*dt,initialCondition,dealias,dataCollectionInterval)
@time ch_problem(nu,dt,N,tMax,initialCondition,dealias,dataCollectionInterval)
nothing




