using BenchmarkTools
using Profile, PProf

function pad_row(uh::Matrix{T}, m::Int64) where T <: Number
    n=size(uh,1);
    k=div(n,2);
    uhp=zeros(T,m,m);

    for i=1:k
        uhp[i,1:k]     = view(uh, i, 1:k  ); 
    end
    for i=1:k
        uhp[i,m-k+1:m] = view(uh, i, k+1:n); 
    end
    for i=1:k
        uhp[m-k+i,1:k]     = view(uh, k+i, 1:k  );
    end
    for i=1:k 
        uhp[m-k+i,m-k+1:m] = view(uh, k+i, k+1:n);
    end
    
    return uhp;
end

function pad_col(uh::Matrix{T}, m::Int64) where T <: Number
    n=size(uh,1);
    k=div(n,2);
    uhp=zeros(T,m,m);

    for j=1:k
        uhp[1:k,j]         = view(uh, 1:k  , j    ); 
        uhp[1:k,m-k+j]     = view(uh, 1:k  , n-k+j); 
        uhp[m-k+1:m,j]     = view(uh, k+1:n, j    );
        uhp[m-k+1:m,m-k+j] = view(uh, k+1:n, n-k+j);
    end
    
    return uhp;
end

function pad_block(uh::Matrix{T}, m::Int64) where T <: Number
    n=size(uh,1);
    k=div(n,2);
    uhp=zeros(T,m,m);

    uhp[1:k,1:k]         = view(uh, 1:k  ,1:k  );
    uhp[1:k,m-k+1:m]     = view(uh, 1:k  ,k+1:n);
    uhp[m-k+1:m,1:k]     = view(uh, k+1:n,1:k  );
    uhp[m-k+1:m,m-k+1:m] = view(uh, k+1:n,k+1:n);

    return uhp;
end

function pad(uh::Matrix{T}, m::Int64) where T <: Number
    n=size(uh,1);
    uhp=zeros(T,m,m);
    for j=1:div(n,2)
        uhp[j,1:div(n,2)]=uh[j,1:div(n,2)]; 
        uhp[j,m-div(n,2)+1:m]=uh[j,div(n,2)+1:n]; 
    end
    for j=1:div(n,2) 
        uhp[m-div(n,2)+j,1:div(n,2)]=uh[div(n,2)+j,1:div(n,2)];
        uhp[m-div(n,2)+j,m-div(n,2)+1:m]=uh[div(n,2)+j,div(n,2)+1:n];
    end
    return uhp;
end

n=1024
A = Vector{ComplexF64}(1:n^2);
A = reshape(A,(n,n));
m=div(3*n,2);

T1 = pad(A,m);
T2 = pad_row(A,m);
T3 = pad_col(A,m);
T4 = pad_block(A,m);
print(T1==T2)
print(T1==T3)
print(T1==T4)
print("\n\n")

io = IOContext(stdout)

pad_row(A,m)
b1 = @benchmark pad_row(a,M) setup=(a=A;M=m); show(io, MIME("text/plain"), b1); print("\n\n")

pad_col(A,m)
b2 = @benchmark pad_col(a,M) setup=(a=A;M=m); show(io, MIME("text/plain"), b2); print("\n\n")

pad_block(A,m)
b3 = @benchmark pad_block(a,M) setup=(a=A;M=m); show(io, MIME("text/plain"), b3);
