using JLD2
using FileIO
using PlotlyJS
using FFTW

@load "data/workspace1.jld2" x y tdata uhatdata dt

z = real(ifft(last(uhatdata)))'

data = heatmap(;x=x, y=y, z=z, zsmooth="best")
layout = Layout(;title="Smooth Contour Coloring")
plot(data, layout)