# Carry out Crank-Nicolson + Adams-Bashforth-2nd-Order timestepping for the Cahn-Hillard equation. Where CN is applied 
# to the linear part and AB2 to the nonlinear part. The spatial discretization method is the Fourier Galerkin Spectral method.

function cahnHillardGalerkinCNAB2(nu::Float64,dt::Float64,N::Integer,tmax::Float64,tdata::Array{Float64,1},uhatdata::Array{Matrix{ComplexF64},1},dealias::Bool,dataCollectionInterval::Float64)
    
    t=last(tdata)
    steps = trunc(tmax/dt)
    j=trunc(t/dt)
    nextCollectionTime = dataCollectionInterval*floor(t/dataCollectionInterval)+dataCollectionInterval

    L=constructLinearOperator(nu,N);
    K=constructLaplacianOperator(N);

    Ukhat=last(uhatdata);
    Nhatold=nonlinearOperator(K,uhatdata[length(uhatdata)-1],dealias);
    
    # using CN/AB2
    for i=j:steps

        Nhat=nonlinearOperator(K,Ukhat,dealias);
        Ukhat=(((dt/2)*L.+1).*Ukhat+(dt/2)*(3*Nhat-Nhatold))./(-(dt/2)*L.+1);
        Nhatold=Nhat;
        t=i*dt
        #print(t)

        if (t >= nextCollectionTime) || (i==steps)
            nextCollectionTime = nextCollectionTime + dataCollectionInterval;
            push!(tdata,t);
            push!(uhatdata,Ukhat);
        end
    end
    return tdata,uhatdata,dt;
end

