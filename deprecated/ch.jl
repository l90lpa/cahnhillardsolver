
using FFTW

include("ch_cnfe.jl")
include("ch_cnab2.jl")
include("../ch_linearOperator.jl")
include("nonlinearOperator.jl")
include("../matlab_functions.jl")

# Cahn-Hillard solver for periodic boundary conditions
function cahnHillardGalerkin(
    nu::Float64,
    dt::Float64,
    N::Integer,
    tmax::Float64,
    ic::Function,
    dealias::Bool,
    dataCollectionInterval::Float64
    )
    
    # Construct 'mesh'
    dx = 2 * pi / N;
    x = dx * collect(1:N);
    y = x;
    X,Y = meshgrid(x,y);
    
    # Sample initial-condition on the mesh and transform to frequency space
    U = ic(X,Y)
    Ukhat = fft(U);

    tdata = Array{Float64, 1}(undef, 1);
    tdata[1]  = 0.0;
    uhatdata = Array{Matrix{ComplexF64},1}(undef,1);
    uhatdata[1] = Ukhat;

    # Using CNFE for the first time step so that we have the 2 time points need to start CNAB2
    tdata,uhatdata,dt = cahnHillardGalerkinCNFE(nu,dt,N,dt,tdata,uhatdata,dealias,dataCollectionInterval);
    tdata,uhatdata,dt = cahnHillardGalerkinCNAB2(nu,dt,N,tmax,tdata,uhatdata,dealias,dataCollectionInterval);

    # tdata:    the time steps that data was recorded at
    # uhatdata: the Fourier coefficients of the solution at the time steps given by tdata
    # dt:       the time step used
    return x,y,tdata,uhatdata,dt
end

