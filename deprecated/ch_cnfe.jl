# Carry out Crank-Nicolson + Forward-Euler timestepping for the Cahn-Hillard equation. Where CN is applied to the linear part
# and FE to the nonlinear part. The spatial discretization method is the Fourier Galerkin Spectral method.

function cahnHillardGalerkinCNFE(nu::Float64,dt::Float64,N::Int64,tmax::Float64,tdata::Array{Float64,1},uhatdata::Array{Matrix{ComplexF64},1},dealias::Bool,dataCollectionInterval::Float64)
    
    t=last(tdata)
    steps = trunc(tmax/dt)
    j=trunc(t/dt)
    nextCollectionTime = dataCollectionInterval*floor(t/dataCollectionInterval)+dataCollectionInterval

    L=constructLinearOperator(nu,N)
    K=constructLaplacianOperator(N)

    Ukhat=last(uhatdata)
    # using CN/FE
    for i=j:steps
        Nhatold=nonlinearOperator(K,Ukhat,dealias)
        Ukhat=(((dt/2)*L.+1).*Ukhat+dt*Nhatold)./(-(dt/2)*L.+1)
        t=i*dt
        #print(t)
    
        if (t >= nextCollectionTime) || (i==steps)
            nextCollectionTime = nextCollectionTime + dataCollectionInterval
            push!(tdata,t)
            push!(uhatdata,Ukhat)
        end
    end
    return tdata,uhatdata,dt;
end



