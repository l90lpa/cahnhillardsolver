using FFTW

# Add a buffer of zeros to a Matrix containing 2D Discrete Fourier coefficients. Remembering that Fouirer coefficients are
# not stored in 'logical' order. See Julia or Matlab docs for more info.
function pad(uh::Matrix{T}, m::Int64) where T <: Number
    n=size(uh,1);
    k=div(n,2);
    uhp=zeros(T,m,m);

    for j=1:k
        uhp[j,    1:k]=uh[j,  1:k]; 
        uhp[j,m-k+1:m]=uh[j,k+1:n]; 
    end
    for j=1:k 
        uhp[m-k+j,    1:k]=uh[k+j,  1:k];
        uhp[m-k+j,m-k+1:m]=uh[k+j,k+1:n];
    end
    
    return uhp;
end

# Truncate Matrix containing 2D Discrete Fourier coefficients. Remembering that Fouirer coefficients are
# not stored in 'logical' order. See Julia or Matlab docs for more info.
function truncate(uhp::Matrix{T}, n::Int64) where T <: Number
    m=size(uhp,1);
    uh=zeros(T,n,n);
    k = div(n,2);
    
    for j=1:k
        uh[j,  :]=1.5*[uhp[j,    1:k] uhp[j,    m-k+1:m]]; 
        uh[k+j,:]=1.5*[uhp[m-k+j,1:k] uhp[m-k+j,m-k+1:m]]; 
    end
    
    return uh;
end

# Pseudospectral method for computing the convolution sum of DFT(u^3) where u is in physical space, and dealiasing is used.
# The idea is that a convolution sum in frequency space is equivalent to a element-wise product in physical space, hence transform
# to physical space, do the product, and then transform back.
# Direct convolution sum: O(N^2)
# Pseudospectral method:  O(N*log(N))
function convolutionDealiased(uh::Matrix{T}) where T <: Complex 
    
    n=size(uh,1);
    m=div(n*3,2);

    # --- convolution sum for u^2 ---

    uhp=pad(uh, m);
    up=ifft(uhp);
    w=up.^2;
    wh=fft(w);
    # extract Fourier n coefficients
    ph=truncate(wh, n);
    ph=1.5*ph;

    # --- convolution sum for u.*(u^2) ---

    uhp2=pad(ph, m);
    up2=ifft(uhp2);
    w=up.*up2;
    wh=fft(w);
    # extract Fourier n coefficients
    ph=truncate(wh, n);
    ph=1.5*ph;
    return ph;
end

# Nonlinear operator part of Cahn-Hillard equation in frequency space 
function nonlinearOperator(laplacianOperator::Matrix{T},Ukhat::Matrix{S},dealias::Bool) where {T <: AbstractFloat, S <: Complex}
    if dealias
        Vhat=convolutionDealiased(Ukhat);
    else
        # Non-dealiased pseudospectral method for convolution sum
        U=real(ifft(Ukhat));
        Vhat=fft(U.^3);
    end
    Nhat = laplacianOperator.*Vhat;
    return Nhat;
end