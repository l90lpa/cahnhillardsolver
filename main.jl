using Random
using JLD2
using FileIO

include("ch_problem.jl")

function initialCondition(X::Matrix{T}, Y::Matrix{T}) where T <: Number
    rng = MersenneTwister(257);
    return 0.01*(2*rand(rng, size(X,1),size(X,2)).-1);
end

nu=0.01;
dt=0.0001;
N=32;
tMax=1.0;
dealias=true;
dataCollectionInterval=0.25;

x,y,tdata,uhatdata,dt=ch_problem(nu,dt,N,tMax,initialCondition,dealias,dataCollectionInterval);

@save "data/workspace1.jld2" x y tdata uhatdata dt