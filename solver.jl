include("timeIntegrators.jl")

function solver(
    integrator::IMEXIntegrator,
    L,
    N::NonLinearOperator,
    dt::Float64,
    tmax::Float64,
    tdata::Array{Float64,1},
    udata::Array{Matrix{ComplexF64},1},
    dataCollectionInterval::Float64
    )
    
    t = last(tdata)
    steps = trunc(tmax/dt)
    j = trunc(t/dt)
    nextCollectionTime = dataCollectionInterval * floor(t / dataCollectionInterval) + dataCollectionInterval

    u = last(udata);
    
    for i=j:steps

        u = step(integrator,dt,u,L,N);
        t = i*dt;
        #print(t)

        if (t >= nextCollectionTime) || (i==steps)
            nextCollectionTime = nextCollectionTime + dataCollectionInterval;
            push!(tdata,t);
            push!(udata,u);
        end
    end
    return tdata,udata,dt;
end