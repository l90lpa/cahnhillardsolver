# Periodic boundary condition, 2D Cahn Hillard Solver
The solver uses an implicit-explicit (IMEX) time-integration (time stepping) method for time discretization, and the Fourier Galerkin spectral method for spatial discretization.  

## Time-discretization
The first time step is computed using Crank-Nicolson/Forward-Euler for the linear and nonlinear parts of the PDE repectively. The remaining time steps are computed using Crank-Nicolson/Adams-Bashforth-2nd-Order for the linear and nonlinear parts of the PDE repectively.

## Spatial-discretization
The Fourier Galerkin spectral method is used for the spatial discretization.

## Code structure
- `solver.jl`: contains a generic method for solving first-order split ODE system, where the right hand side is split into a linear, and a nonlinear operator.
- `timeIntegrators.jl`: contains generic time integration routines CNFE and CNAB2 for time stepping a split linear/nonlinear ODE system.
- `ch_linearOperator` and `ch_nonlinearOperator.jl`: contain the linear and nonlinear operators of the Cahn-Hillard problem
- `ch_problem.jl`: wraps together creation of the operators, and chosen time integration routine with running the solver. It is the primary method for solving the problem.
- `main.jl` contains an example of using the solver.
- `matlab_functions.jl` contains ports of functions found in Matlab.