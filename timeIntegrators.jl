abstract type NonLinearOperator end

abstract type IMEXIntegrator end

mutable struct CNFE <:  IMEXIntegrator end

function step(integrator::CNFE, dt, u, L, N::NonLinearOperator)
    v = map(N, u);
    dt2 = dt/2;
    u = ((((L .* dt2) .+ 1) .* u) .+ (v .* dt)) ./ ((L .* (-dt2)) .+ 1);
    return u;
end

mutable struct CNAB2 <: IMEXIntegrator 
    v_cached;

    function CNAB2(u, N::NonLinearOperator)
        v = map(N, u);
        new(v);
    end
end

function step(integrator::CNAB2, dt, u, L, N::NonLinearOperator)
    v = map(N, u);
    dt2 = dt/2;
    u = ((((L .* dt2) .+ 1) .* u) .+ ((v .* 3) .- integrator.v_cached) .* dt2) ./ ((L .* (-dt2)) .+ 1);
    integrator.v_cached = v;
    return u;
end