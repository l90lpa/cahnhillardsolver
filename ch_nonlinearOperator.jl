using FFTW
include("timeIntegrators.jl")
include("ch_linearOperator.jl")

struct CH_NLOperator <: NonLinearOperator
    n::Int64
    m::Int64
    k::Int64
    Laplacian::Matrix{Float64}
    dealias::Bool

    function CH_NLOperator(n, dealias)
        m=div(3*n,2);
        k=div(n,2);

        L=constructLaplacianOperator(n);

        new(n,m,k,L,dealias)
    end
end


# Add a buffer of zeros to a Matrix containing 2D Discrete Fourier coefficients. Remembering that Fouirer coefficients are
# not stored in 'logical' order. See Julia or Matlab docs for more info.
function pad(uh::Matrix{T}, m::Int64) where T <: Number
    n=size(uh,1);
    k=div(n,2);
    uhp=zeros(T,m,m);

    for j=1:k
        uhp[1:k,j]         = view(uh, 1:k  , j    ); 
        uhp[1:k,m-k+j]     = view(uh, 1:k  , n-k+j); 
        uhp[m-k+1:m,j]     = view(uh, k+1:n, j    );
        uhp[m-k+1:m,m-k+j] = view(uh, k+1:n, n-k+j);
    end
    
    return uhp;
end

# Truncate Matrix containing 2D Discrete Fourier coefficients. Remembering that Fouirer coefficients are
# not stored in 'logical' order. See Julia or Matlab docs for more info.
function truncate(uhp::Matrix{T}, n::Int64) where T <: Number
    m=size(uhp,1);
    uh=zeros(T,n,n);
    k = div(n,2);

    for j=1:k
        uh[1:k  ,j]   = view(uhp,1:k    ,j); 
        uh[k+1:n,j]   = view(uhp,m-k+1:m,j); 
        uh[1:k  ,k+j] = view(uhp,1:k    ,m-k+j); 
        uh[k+1:n,k+j] = view(uhp,m-k+1:m,m-k+j); 
    end
    
    return uh;
end

# Pseudospectral method for computing the convolution sum of DFT(u^3) where u is in physical space, and dealiasing is used.
# The idea is that a convolution sum in frequency space is equivalent to a element-wise product in physical space, hence transform
# to physical space, do the product, and then transform back.
# Direct convolution sum: O(N^2)
# Pseudospectral method:  O(N*log(N))
function convolutionDealiased(uh::Matrix{T}) where T <: Complex 
    
    n=size(uh,1);
    m=div(n*3,2);

    # --- convolution sum for u^2 ---

    uhp=pad(uh, m);
    up=ifft(uhp);
    w=up.^2;
    wh=fft(w);
    # extract Fourier n coefficients
    ph=truncate(wh, n);
    ph=1.5*ph;

    # --- convolution sum for u.*(u^2) ---

    uhp2=pad(ph, m);
    up2=ifft(uhp2);
    w=up.*up2;
    wh=fft(w);
    # extract Fourier n coefficients
    ph=truncate(wh, n);
    ph=1.5*ph;
    return ph;
end

# Nonlinear operator part of Cahn-Hillard equation in frequency space 
function map(op::CH_NLOperator, A::Matrix{ComplexF64})
    if op.dealias
        V=convolutionDealiased(A);
    else
        # Non-dealiased pseudospectral method for convolution sum
        U=real(ifft(A));
        V=fft(U.^3);
    end
    
    return op.Laplacian .* V;
end


## This approach has found to be better for, N > 64

mutable struct CH_NLOperatorCached <: NonLinearOperator
    B::Matrix{ComplexF64}
    C::Matrix{ComplexF64}
    U::Matrix{Float64}
    n::Int64
    m::Int64
    k::Int64
    ids
    Laplacian::Matrix{Float64}
    dealias::Bool

    function CH_NLOperatorCached(n, dealias)
        m=div(3*n,2);
        k=div(n,2);

        L=constructLaplacianOperator(n);

        ids = collect(1:k);
        append!(ids, m-k+1:m);

        B=Matrix{ComplexF64}(undef, (m,m));
        C=Matrix{ComplexF64}(undef, (m,m));
        U=Matrix{Float64}(undef, (n,n));

        new(B,C,U,n,m,k,ids,L,dealias)
    end
end

function map(op::CH_NLOperatorCached, A::Matrix{ComplexF64})

    out = similar(A);
    if op.dealias
        n=op.n;
        k=op.k;
        m=op.m;
    
        op.B .= 0;
        op.B[1:k,1:k]         = view(A, 1:k  ,1:k  );
        op.B[1:k,m-k+1:m]     = view(A, 1:k  ,k+1:n);
        op.B[m-k+1:m,1:k]     = view(A, k+1:n,1:k  );
        op.B[m-k+1:m,m-k+1:m] = view(A, k+1:n,k+1:n);
    
        ifft!(op.B);
        op.C .= op.B .^ 2;
        fft!(op.C);
    
        op.C[op.ids,op.ids] .*= 1.5;
        op.C[1:k, k+1:m-k]     .= 0;
        op.C[k+1:m-k, 1:m]     .= 0;
        op.C[m-k+1:m, k+1:m-k] .= 0;
    
        ifft!(op.C);
        op.C .*= op.B;
        fft!(op.C);
    
        out[:,:] = view(op.C, op.ids,op.ids);
        out .*= 1.5;
    else
        # Non-dealiased pseudospectral method for convolution sum
        op.U = real(ifft(A));
        out = fft(op.U .^ 3);
    end

    out .*= op.Laplacian;
    return out;
end
