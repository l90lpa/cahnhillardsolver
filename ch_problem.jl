include("solver.jl")
include("ch_nonlinearOperator.jl")

function ch_problem(
    nu::Float64,
    dt::Float64,
    n::Integer,
    tmax::Float64,
    ic::Function,
    dealias::Bool,
    dataCollectionInterval::Float64
    )
    
    # Construct 'mesh'
    dx = 2 * pi / n;
    x = dx * collect(1:n);
    y = x;
    X,Y = meshgrid(x,y);
    
    # Sample initial-condition on the mesh and transform to frequency space
    U = ic(X,Y)
    Ukhat = fft(U);

    tdata = zeros(Float64, 1);
    uhatdata = Array{Matrix{ComplexF64},1}(undef,1);
    uhatdata[1] = Ukhat;

    L = constructLinearOperator(nu,n)
    N = CH_NLOperatorCached(n, dealias)
    
    # Using CNFE for the first time step so that we have the 2 time points need to start CNAB2
    integrator = CNFE();
    tdata,uhatdata,dt = solver(integrator,L,N,dt,tmax,tdata,uhatdata,dataCollectionInterval)
    
    integrator = CNAB2(uhatdata[length(uhatdata)-1], N)
    tdata,uhatdata,dt = solver(integrator,L,N,dt,tmax,tdata,uhatdata,dataCollectionInterval)

    # tdata:    the time steps that data was recorded at
    # uhatdata: the Fourier coefficients of the solution at the time steps given by tdata
    # dt:       the time step used
    return x,y,tdata,uhatdata,dt
end