include("matlab_functions.jl")

# Laplacian operator in frequency space
function constructLaplacianOperator(N::Integer)
    p = cat(0:div(N,2), -div(N,2)+1:-1, dims=1)
    q = cat(0:div(N,2), -div(N,2)+1:-1, dims=1)
    p2 = p .^ 2;
    q2 = q .^ 2; 
    pp2,qq2 = ndgrid(p2 ,q2);
    return convert(Matrix{Float64}, -(pp2 + qq2))
end

# Linear operator part of Cahn-Hillard equation in frequency space 
function constructLinearOperator(nu::AbstractFloat,N::Integer)
    K=constructLaplacianOperator(N);
    return -(K + nu * K .^ 2);
end